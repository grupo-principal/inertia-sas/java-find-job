package functional.functional;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class CLIArgumentsUtils {
    static void showHelp(CLIArguments cliArguments){
        Consumer<CLIArguments> consumerHelpers = cliArgument -> {
            if(cliArgument.isHelp()){
                System.out.println("Manual Solicitado");
            }
        };

        consumerHelpers.accept(cliArguments);
    }
    static CLIArguments generateCLI(){
        Supplier<CLIArguments> generator = () -> new CLIArguments();
        return generator.get();
    };
}
